package main

import (
	"mailgun_events_exporter/internal/config"
	"mailgun_events_exporter/internal/exporter"
	"net/http"

	"github.com/alecthomas/kingpin/v2"
	"github.com/go-kit/log/level"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/prometheus/common/promlog"
	"github.com/prometheus/common/promlog/flag"
	"github.com/prometheus/common/version"
)

func main() {
	promlogConfig := &promlog.Config{}
	flag.AddFlags(kingpin.CommandLine, promlogConfig)

	var (
		listenAddress = kingpin.Flag("web.listen-address", "Address to listen on for web interface and telemetry.").Default(":2112").String()
		metricsPath   = kingpin.Flag("web.telemetry-path", "Path under which to expose metrics.").Default("/metrics").String()
	)

	kingpin.Version(version.Print("prometheus-mailgun-events-exporter"))
	kingpin.HelpFlag.Short('h')
	kingpin.Parse()

	logger := promlog.New(promlogConfig)
	level.Info(logger).Log("msg", "Starting prometheus-mailgun-events-exporter", "version", version.Info())
	level.Info(logger).Log("msg", "Build context", "build_context", version.BuildContext())
	level.Info(logger).Log("msg", "Starting HTTP server", "listen address", *listenAddress, "metrics path", *metricsPath)

	cfg := config.FromEnv()
	if err := cfg.Validate(); err != nil {
		if err != nil {
			level.Error(logger).Log("msg", "invalid config", "error", err)
			return
		}
	}

	exporter := exporter.NewExporter(cfg)
	exporter.Start()

	http.Handle(*metricsPath, promhttp.Handler())
	if err := http.ListenAndServe(*listenAddress, nil); err != nil {
		level.Error(logger).Log("msg", "Error starting server", "error", err)
	}
}
