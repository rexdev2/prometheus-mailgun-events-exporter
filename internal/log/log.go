package log

import (
	"log/slog"
	"mailgun_events_exporter/internal/config"
	"os"
)

func NewLogger(cfg *config.Config) *slog.Logger {
	options := &slog.HandlerOptions{
		Level: ToLogLevel(cfg.LogLevel),
	}

	return slog.New(slog.NewJSONHandler(os.Stdout, options))
}

func ToLogLevel(logLevel string) slog.Level {
	switch {
	case logLevel == "debug":
		return slog.LevelDebug
	case logLevel == "warn":
		return slog.LevelWarn
	case logLevel == "info":
		return slog.LevelInfo
	case logLevel == "error":
		return slog.LevelError
	default:
		return slog.LevelInfo
	}
}
