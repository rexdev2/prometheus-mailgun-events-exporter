package exporter

import (
	"fmt"
	"log/slog"
	"mailgun_events_exporter/internal/config"
	"mailgun_events_exporter/internal/log"
	"mailgun_events_exporter/internal/metrics"
	"mailgun_events_exporter/pkg/mailgun"
	"time"

	"github.com/mailgun/mailgun-go/v4/events"
	"github.com/zekroTJA/timedmap"
)

type Exporter struct {
	started bool
	cfg     *config.Config
	ticker  *time.Ticker
	tm      *timedmap.TimedMap
	logger  *slog.Logger
	mg      mailgun.EventRetriever
}

type ExporterOption func(*Exporter)

const (
	DEFAULT_RETENTION_DURATION = 10 * time.Hour
)

func messageKey(messageId string, recipient string) string {
	return fmt.Sprintf("%s/%s", messageId, recipient)
}

func WithEventRetriever(mg mailgun.EventRetriever) ExporterOption {
	return func(e *Exporter) {
		e.mg = mg
	}
}

func WithTicker(ticker *time.Ticker) ExporterOption {
	return func(e *Exporter) {
		e.ticker = ticker
	}
}

func NewExporter(cfg *config.Config, opts ...ExporterOption) *Exporter {
	e := &Exporter{
		started: false,
		cfg:     cfg,
		ticker:  time.NewTicker(time.Minute),
		tm:      timedmap.New(1 * time.Minute),
		logger:  log.NewLogger(cfg),
		mg:      mailgun.NewMailgunRetriever(cfg),
	}

	for _, opt := range opts {
		opt(e)
	}

	return e
}

func (e *Exporter) Start() {
	if !e.started {
		go e.recordMetrics()
		e.started = true
	}
}

func (e *Exporter) recordMetrics() {
	for ; true; <-e.ticker.C {
		accepted, delivered, failed, err := e.mg.GetEvents()
		if err != nil {
			e.logger.Error(err.Error())
		}

		e.recordAccepted(accepted)
		e.recordDelivered(delivered)
		e.recordFailed(failed)
		e.updateTimedMapCount()
	}
}

func (e *Exporter) updateTimedMapCount() {
	metrics.QueuedAcceptedEvents.WithLabelValues(e.cfg.Domain).Set(float64(e.tm.Size()))
}

func (e *Exporter) keyExpiredCallback() func(value interface{}) {
	return func(value interface{}) {
		e.logger.Debug("Dropping accepted event with message ID", "message_id", value)
		metrics.ExpiredAcceptedEvents.WithLabelValues(e.cfg.Domain).Inc()
	}
}

func (e *Exporter) recordAccepted(accepted []*events.Accepted) {
	for _, a := range accepted {
		key := messageKey(a.Message.Headers.MessageID, a.Recipient)
		if e.tm.GetValue(key) == nil {
			// Only increment the accepted counter if the message
			// has not been seen yet
			metrics.DeliveryAccepted.WithLabelValues(e.cfg.Domain).Inc()
		}
		e.tm.Set(key, a.Timestamp, DEFAULT_RETENTION_DURATION, e.keyExpiredCallback())
	}
}

func (e *Exporter) recordDelivered(delivered []*events.Delivered) {
	for _, d := range delivered {
		key := messageKey(d.Message.Headers.MessageID, d.Recipient)
		acceptedTime, ok := e.tm.GetValue(key).(float64)
		if !ok {
			continue
		}

		e.tm.Remove(key)

		delta := d.Timestamp - acceptedTime
		if delta < 0 {
			e.logger.Error("Delivery time is negative")
			continue
		}

		e.logger.Debug("Delivery Succeeded", "message_id", d.Message.Headers.MessageID, "accepted_at", acceptedTime, "delivered_at", d.GetTimestamp(), "delivery_time_s", delta)
		metrics.DeliveryTime.WithLabelValues(e.cfg.Domain).Observe(delta)
	}
}

func (e *Exporter) recordFailed(failed []*events.Failed) {
	for _, f := range failed {
		severity := f.Severity

		if severity == "permanent" {
			key := messageKey(f.Message.Headers.MessageID, f.Recipient)
			e.tm.Remove(key)
		}

		e.logger.Debug("Delivery Failed", "message_id", f.Message.Headers.MessageID, "failed_at", f.GetTimestamp(), "reason", f.Reason, "error_message", f.DeliveryStatus.Message, "Failure Severity", severity)
		metrics.DeliveryError.WithLabelValues(e.cfg.Domain, f.Reason, severity, fmt.Sprintf("%d", f.DeliveryStatus.Code)).Inc()
	}
}
