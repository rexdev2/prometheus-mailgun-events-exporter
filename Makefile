.PHONY: lint test test race

OUT_FORMAT ?= colored-line-number
LINT_FLAGS ?=  $(if $V,-v)
REPORT_FILE ?=
GOLANGCI_VERSION=v1.52.2
GOTESTSUM_VERSION=v1.10.0

buildlinux:
	CGO_ENABLED=0 GOOS=linux go build -o ./bin/mailgun_events_exporter-linux ./cmd/*

darwinbuild:
	CGO_ENABLED=0 GOOS=darwin go build -o ./bin/mailgun_events_exporter-darwin ./cmd/*

build: buildlinux darwinbuild

build-image:
	docker build -t gathertown/prometheus-mailgun-events-exporter:latest .

lint:
	go run github.com/golangci/golangci-lint/cmd/golangci-lint@$(GOLANGCI_VERSION) run ./... --out-format $(OUT_FORMAT) $(LINT_FLAGS) | tee ${REPORT_FIL\
E}

test:
	go run gotest.tools/gotestsum@$(GOTESTSUM_VERSION) --junitfile junit-test-report.xml --format testname -- -short ./... ${ARGS}

race:
	go run gotest.tools/gotestsum@$(GOTESTSUM_VERSION) --junitfile junit-test-report.xml --format testname -- -race -v ./... ${ARGS}

run: build
	go run ./cmd/main.go

.PHONY: fmt
fmt:
	go fmt ./...

.PHONY: clean
clean:
	rm -rf bin/
