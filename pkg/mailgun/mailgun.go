package mailgun

import (
	"context"
	"fmt"
	"log/slog"
	"mailgun_events_exporter/internal/config"
	"mailgun_events_exporter/internal/log"
	"time"

	"github.com/mailgun/mailgun-go/v4"
	"github.com/mailgun/mailgun-go/v4/events"
)

type EventRetriever interface {
	GetEvents() ([]*events.Accepted, []*events.Delivered, []*events.Failed, error)
}

type MailgunRetriever struct {
	EventRetriever

	mg     *mailgun.MailgunImpl
	logger *slog.Logger
}

func NewMailgunRetriever(cfg *config.Config) *MailgunRetriever {
	return &MailgunRetriever{
		mg:     mailgun.NewMailgun(cfg.Domain, cfg.ApiKey),
		logger: log.NewLogger(cfg),
	}
}

func (r *MailgunRetriever) GetEvents() ([]*events.Accepted, []*events.Delivered, []*events.Failed, error) {
	accepted := []*events.Accepted{}
	delivered := []*events.Delivered{}
	failed := []*events.Failed{}

	it := r.mg.ListEvents(&mailgun.ListEventOptions{
		Begin: time.Now().Add(-3 * time.Minute),
		End:   time.Now().Add(-2 * time.Minute),
		Limit: 300,
	})

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*30)
	defer cancel()

	// Iterate through all the pages of events
	var page []mailgun.Event
	for it.Next(ctx, &page) {
		for _, event := range page {
			switch e := event.(type) {
			case *events.Accepted:
				accepted = append(accepted, e)
			case *events.Delivered:
				delivered = append(delivered, e)
			case *events.Failed:
				failed = append(failed, e)
			}
		}
	}

	if it.Err() != nil {
		r.logger.Error(it.Err().Error())
		fmt.Println(it.Err())
		return nil, nil, nil, it.Err()
	}
	r.logger.Debug("Total Events last minute", "accepted", len(accepted), "delivered", len(delivered), "failed", len(failed))
	return accepted, delivered, failed, nil
}
